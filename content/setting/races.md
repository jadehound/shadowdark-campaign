+++
title = "Races"
description = ""
weight = 1
[extra] 
cover_image = "images/mouse-family.webp"
+++

## Human

**Names.** Based on old English.

**Ambitious.** You gain one additional talent roll at 1st level.

## Goblin

**Names.** Based on this
<a href="https://www.namesnerd.com/fantasy/goblin-name-generator" target="_blank">generator</a>.

**Chaotic.** Goblins are chaotic creatures at heart, forgoing careful planning
and preparation in favour of being guided by an almost supernatural intuition.

**Adaptable.** They are also incredibly adaptable, their very bodies conforming
to the environment they find themselves in (e.g. goblins that live in the
mountains have pale, thicker skin, able to survive blows that would maim most).

- Danger Sense (Forest). Cannot be surprised.

**Classes.** Can be fighters, rogues or lawful clerics.

**History.** Goblins live in tight-knit family groups and seldom travel alone.
Their love of rare metals, shiny stones and hand-crafted goods often leads them
into conflict with the other races, especially humans. Tribes that have
integrated in human society tend to be more prosperous than those that choose to
live in the wilds, but are treated poorly humans they live alongside.

## Elf

Names. Based on Celtic.

Lightborne. Elves have a deep connection to light magic. They shed faint light
from their irises and a whole body tattoo like birthmark.

Roll spellcasting checks with advantage whenever you cast any light based spell.

Pacifists. Elves abhor violence and raise the blade in defence only.

Classes. Can be either a cleric or wizard.

History. Elves and humans were once allies but when humanity started twisting
their magic for warfare the elves distanced themselves, retreating to their
respective Lightwells.

## Halfling

Burrow. Retractable blunt claws that can burrow in loamy soil.
