+++
title = "Gods"
description = ""
weight = 2
[extra] 
cover_image = "images/phenax.webp"
+++

# Human Pantheon

## Bellathor, Jackal of War and Courage

_"We follow the bell, we serve the fang."_

Manifests as a anthromorphic jackal wielding a large bell as a mace and the
symbol of his order is that of a golden bell in the middle of a bright sun.

The clergymen of Ballathor make public demonstrations of martial prowess as an
offering; especially in times of war.

# Elvish Pantheon

## Celestine, God of the Justice

Appears in the form of a hawk-headed knight. Although originally an elvish
diety, Celestine's small yet militant following amongst the humans has largely
been what has defined her in the current age.
