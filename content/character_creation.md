+++
title = "Character Creation"
description = "A quick guide to character creation."
weight = 1
[extra] 
cover_image = "images/the-adventure-begins.webp"
+++

1. Determine [ability scores](@/rules/ability_scores.md) by rolling for them
1. Choose a [background](@/rules/backgrounds.md)
   - Rolling randomly for a background can more exciting than simply choosing
     one; afterall the backstory of how a regal elf became a dung-merchant is
     rife with exciting prospectives
1. Choose a [race](@/setting/races.md)
   - Each race has it's own benefits and drawbacks, make sure you note yours
     down
   - If you are unhappy with the choices available, chat with the DM to
     potentially create a new one; just make sure you are ready to do a little
     bit of work to properly flesh them out
1. Choose [class](@/classes/_index.md)
   - Classes define your current skillset and will improve as you
     [level up](@/rules/levels.md)
   - Try to choose a class that matches your ability scores, for instance it
     might not be the best idea to be a [Fighter](@/classes/fighter.md) if you
     aren't very strong
1. Roll for a class talent
   - As you level up, you get more talents; in other words talents make you more
     specialised in the class you've chosen
1. Roll for Hitpoints: HP = Roll class hit point dice + constitution modifier
   - A typical monster rolls 1d6 damage if they hit you
1. Describe your character through [traits](@/misc/traits.md)
   - Surprise yourself by rolling randomly here as well
   - Try to draw your character or get
     [Bing AI (Desktop Only)](https://www.bing.com/search?q=Bing+AI&showconv=1&FORM=hpcodx)
     to generate what you might look like
1. Choose your equipment: you have 3d6 x 20 copper pieces to
   [buy items](@/rules/items.md)
