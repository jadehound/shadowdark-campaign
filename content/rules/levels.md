+++
title = "Levels"
description = "Your character's growth in numerical form."
weight = 8
[extra] 
cover_image = "images/tavern.webp"
+++

## Experience (XP)

Experience represents your learning, influence and increasing skill and levels
are distinct points on that journey where you gain tangible benefits.

## Level Advancement

To gain a level, you need to earn your

```
current level x 1000 XP
```

**Gold as XP.** The most common way to gain XP is to _waste_ coin, gaining 1 XP
for each copper piece spent this way. An act is considered _wasteful_ if it does
not directly benefit your character in any way.

Try to waste your coin in a way that reinforces your character's motivations.
For instance, the local drunk might spend his coin by organising a party at the
tavern or a cleric might send funds on establishing a temple in the area.

Once you reach a new level:

1. Reset your XP back to 0
1. Roll your class's hit points die and add it to your maximum HP
1. On **odd** levels, roll for a class talent
1. If your a spellcaster, learn new spells if your class spell table allows you
   to
