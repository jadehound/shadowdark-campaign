+++
title = "Exploration"
description = ""
weight = 3
[extra] 
cover_image = "images/treasure-map.webp"
+++

## Watches

While traveling, days are divided into six four-hour watches: three for day,
three for night. Most major actions (traveling, foraging, searching etc.) take
one watch to complete.

## Travel Speed

You can move one six-mile hex per watch, up to three times per day. Each watch
they travel after the third adds 1 fatigue to your inventory. Speed is halved in
darkness, difficult terrain, or severe weather and doubled when riding.

## Navigation

If the terrain or weather while traveling is disorienting, the GM may require a
WIS check of the party’s leader (which the GM rolls in secret) to see if they
move to a random adjacent hex.

## Exploring

The party can spend a watch exploring the area of a six-mile hex to reveal any
areas of interest (such as an overgrown ruin, hidden pool, etc.) that wouldn’t
be noticed by passing through.

## Secret Features

Secret features (such as buried treasure or a hidden door into a mountain)
should have corresponding clues revealed by exploring the hex. They can only be
found by the PCs interacting with the game world.

## Foraging

Finding food takes a watch and requires passing a WIS check, with modifiers for
weather, terrain, etc. On a success, a PC collects d6 rations.

## Travel Hazards

At the end of each watch, roll the Travel Hazard Die and apply the result.

| d6  | Result                                                                                               |
| --- | ---------------------------------------------------------------------------------------------------- |
| 1   | Encounter: something waylays your path                                                               |
| 2   | Fatigue: each party member takes damage unless they spend the next watch resting                     |
| 3   | Depletion: roll a d6 for each perishable item (rations, monster parts, etc.), on a 1 it has gone bad |
| 4   | Travel shift: the weather changes or a local event begins                                            |
| 5   | Sign: the players find a sign that an encounter or a hidden item is nearby                           |
| 6   | Free: no effect                                                                                      |

# Dungeon Delving

## Turns

While dungeon delving, time is tracked in 10-minute segments called turns. Most
actions (moving, searching, fighting, resting, etc.) take up one turn.

## Speed

There are 3 modes of travel speed while in a dungeon:

- Crawling: Automatically detect traps and are able to map their environment
- Walking: Surprised by all encounters, spring all traps but can still map; best
  used as a means to backtrack
- Running: Surprised by all encounters, spring all traps, cannot map and candles
  burn out; mostly used for escapes

## Light and Darkness

**Darkness.** Checks involving movement or coordination have a -10 penalty, you
are surprised by all encounters, spring every trap and cannot map the
environment.

It’s important to never be caught in total darkness, so bring plenty of light
sources. Typically having candles and lanterns as backup to torches. All light
sources below last for 1 hour of real-world time.

**Torches.** Sheds bright light (40'/10'). Allows for faster searching due to
bright light but burns out when depletion is rolled as a hazard.

**Candles.** Sheds dim light (20'/5').

**Lanterns.** Sheds dim light (20'/5').The case allows the light to be directed
and prevents it from being blown out.

## Searching

One turn spent searching a room reveals any non-obvious features (a statuette
inside a drawer, a cracked tile, etc.) as well as any clues to secrets. In dim
light like candlelight or lantern light, this takes two turns instead.

## Dungeons Hazards

At the end of each turn, roll the Dungeon Hazard Die and apply the result.

| d6  | Hazard                                                                           |
| --- | -------------------------------------------------------------------------------- |
| 1   | encounter                                                                        |
| 2   | fatigue: Each party member takes damage unless they spend the next watch resting |
| 3   | Burn: lit torches begin to flicker out                                           |
| 4   | dungeon shift: the dungeon environment changes                                   |
| 5   | sign: The players find a sign that an encounter or a hidden item is nearby       |
| 6   | free: No effect                                                                  |
