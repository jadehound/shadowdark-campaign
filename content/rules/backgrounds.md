+++
title = "Backgrounds"
description = "Your history and past experience."
weight = 9
[extra] 
cover_image = "images/kid-merchant.webp"
+++

**Knowledge and Skills.** Your character had some sort of background before they
decided to embark on their current adventure. The skills and knowledge they
learnt from such a history will be invaluable in the trials ahead.

_Whenever there is a situation where your skills can come in handy let the DM
know; it might just give you the edge you needed._

| d100 | Profession         |
| ---- | ------------------ |
| 1    | Armourer           |
| 2    | Astrologer         |
| 3    | Bag-maker          |
| 4    | Baker              |
| 5    | Barber-Surgeon     |
| 6    | Basket-maker       |
| 7    | Belt-maker         |
| 8    | Blacksmith         |
| 9    | Brasiers           |
| 10   | Brewer             |
| 11   | Bridle-maker       |
| 12   | Embroiderer        |
| 13   | Broom-maker        |
| 14   | Butcher            |
| 15   | Carder             |
| 16   | Carpenter          |
| 17   | Cartwright         |
| 18   | Chalk cutter       |
| 19   | Chandler           |
| 20   | Charcoal Burner    |
| 21   | Cheese-maker       |
| 22   | Clerk              |
| 23   | Cobbler            |
| 24   | Cook               |
| 25   | Cooper             |
| 26   | Dog breeder        |
| 27   | Drunkard           |
| 28   | Dyer               |
| 29   | Farrier            |
| 30   | Felt-maker         |
| 31   | Fisherman          |
| 32   | Fletcher           |
| 33   | Furbisher          |
| 34   | Gambler            |
| 35   | Glassblower        |
| 36   | Goatherd           |
| 37   | Gongfarmer         |
| 38   | Gravedigger        |
| 39   | Hunter             |
| 40   | Jailer             |
| 41   | Jeweler            |
| 42   | Leatherworker      |
| 43   | Link Boy           |
| 44   | Mason              |
| 45   | Mercer             |
| 46   | Midwife            |
| 47   | Miller             |
| 48   | Miner              |
| 49   | Minstrel           |
| 50   | Miracle Play Actor |
| 51   | Needlemaker        |
| 52   | Pack Handler       |
| 53   | Painter            |
| 54   | Parchment maker    |
| 55   | Parent             |
| 56   | Plasterer          |
| 57   | Porter             |
| 58   | Potter             |
| 59   | Poultry-keeper     |
| 60   | Prostitute         |
| 61   | Rag Man            |
| 62   | Rat Catcher        |
| 63   | Rope-maker         |
| 64   | Salter             |
| 65   | Scullion           |
| 66   | Sculptor           |
| 67   | Servant (Domestic) |
| 68   | Servant (Military) |
| 69   | Shepherd           |
| 70   | Shipwright         |
| 71   | Stonecutter        |
| 72   | Swineherd          |
| 73   | Tailor             |
| 74   | Tanner             |
| 75   | Thatcher           |
| 76   | Trumpeter          |
| 77   | Weaponsmith        |
| 78   | Wet Nurse          |
| 79   | Woodcarver         |
| 80   | Woodcutter         |
| 81   | Woolwinder         |
| 82   | Alchemist          |
| 83   | Apothecary         |
| 84   | Architect          |
| 85   | Banker             |
| 86   | Bookbinder         |
| 87   | Bureaucrat         |
| 88   | Chirurgeon         |
| 89   | Clock-maker        |
| 90   | Engineer           |
| 91   | Falconer           |
| 92   | Goldsmith          |
| 93   | Herbalist          |
| 94   | Illustrator        |
| 95   | Lead Servant       |
| 96   | Locksmith          |
| 97   | Poet               |
| 98   | Sailor             |
| 99   | Storyteller        |
| 100  | Tax Collector      |
