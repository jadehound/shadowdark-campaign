+++
title = "Items"
description = "A wise traveller prepares only what allows him to go into the dark and coming out alive."
weight = 1
[extra] 
cover_image = "images/blacksmiths-shop.webp"
+++

## Quickstart: Crawling Kit

A crawling kit contains what most adventures require. It costs 90 cp and uses 7
gear slots. Buy a [weapon](#weapons) and [armour](#armour) to round out your
equipment list.

- Tinderbox
- 2 x Torches (1 hour)
- 3 x Rations (1 day)
- Iron Spikes (10 pack)
- Grappling hook
- Rope, 50 ft.

## Weapons

| Weapon        | Cost | Damage   | Properties        |
| ------------- | ---- | -------- | ----------------- |
| Bastard sword | 20   | 1d8/1d10 | 2 slots           |
| Club          | 5    | 1d4      |                   |
| Crossbow      | 15   | 1d8      | 2 handed, Loading |
| Dagger        | 5    | 1d4      | Finesse, thrown   |
| Greataxe      | 20   | 1d8/1d10 | 2 slots           |
| Greatsword    | 30   | 1d12     | 2 slots, 2 handed |
| Javelin       | 1    | 1d4      | Thrown only       |
| Longbow       | 15   | 1d6      | 2 handed          |
| Mace          | 10   | 1d6      |                   |
| Shortbow      | 10   | 1d4      | 2 handed          |
| Spear         | 5    | 1d6      | Thrown            |
| Staff         | 5    | 1d4      | 2 handed          |
| Warhammer     | 20   | 1d10     | 2 handed          |
| Arrows (20)   | 5    |          |                   |

**Loading.** You must forgo moving to reload this weapon.

**Finesse.** You may use your STR or DEX when attacking with this weapon.

**Thrown.** You can make a ranged attack using either STR or DEX.

**Multiple damage dice.** If a weapon has two damage dice listed, the first is
for one-handed and the second is for 2 handed.

## Armour

| Item       | Cost | Slots | AC  | Properties                         |
| ---------- | ---- | ----- | --- | ---------------------------------- |
| Shield     | 40   | 1     | +1  | Held                               |
| Gambeson   | 40   | 1     | 12  |                                    |
| Brigandine | 500  | 1     | 13  |                                    |
| Chain      | 1200 | 2     | 14  | Disadv. on stealth and swimming    |
| Half plate | 4000 | 3     | 15  | No swimming and disadv. on stealth |
| Full plate | 8000 | 3     | 16  | No swimming and disadv. on stealth |
| Mithril    | x4   | -1    |     | No penalty to stealth or swimming  |

## Light

| Item                        | Cost |
| --------------------------- | ---- |
| Candle, 2 hours (dim light) | 1    |
| Lantern                     | 30   |
| Lantern Oil, 2 hours        | 1    |
| Tinderbox                   | 10   |
| Torch, 1 hour               | 1    |

## Adventuring Items

| Tools & Gear            | Cost |
| ----------------------- | ---- |
| Air Bladder             | 5    |
| Bear Trap               | 20   |
| Bedroll                 | 10   |
| Bellows                 | 10   |
| Black Grease            | 1    |
| Block and Tackle        | 30   |
| Book (Blank)            | 300  |
| Book (Reading)          | 600  |
| Bottle/Vial             | 1    |
| Bucket                  | 5    |
| Caltrops (bag)          | 10   |
| Cards with an extra Ace | 5    |
| Chain (10 ft)           | 10   |
| Chalk (10 pieces)       | 1    |
| Chisel                  | 5    |
| Cookpots                | 10   |
| Crowbar                 | 10   |
| Drill                   | 10   |
| Face Paint/Makeup       | 10   |
| Fake Jewels             | 50   |
| Fishing Rod/Tackle      | 10   |
| Glass Marbles (bag)     | 5    |
| Glue (bottle)           | 1    |
| Grappling Hook          | 10   |
| Hammer                  | 10   |
| Holy Water              | 25   |
| Horn                    | 10   |
| Hourglass               | 300  |
| Incense (packet)        | 10   |
| Iron Tongs              | 10   |
| Ladder (10 ft)          | 10   |
| Large Sponge            | 5    |
| Lens                    | 100  |
| Lockpicks               | 100  |
| Manacles                | 10   |
| Metal File              | 5    |
| Mirror (small, silver)  | 200  |
| Musical Instrument      | 200  |
| Nails (12)              | 5    |
| Net                     | 10   |
| Oilskin Bag             | 5    |
| Oilskin Trousers        | 10   |
| Padlock and Key         | 20   |
| Perfume                 | 50   |
| Pick                    | 10   |
| Pole (10ft)             | 5    |
| Quill and Ink           | 1    |
| Rope (50ft)             | 10   |
| Sack                    | 1    |
| Saw                     | 10   |
| Set of Loaded Dice      | 5    |
| Shovel                  | 10   |
| Small Bell              | 20   |
| Soap                    | 1    |
| Spike (iron)            | 5    |
| Spike (wood)            | 1    |
| Spiked boots            | 5    |
| Spyglass                | 1000 |
| Tar (Pot)               | 10   |
| Tent (3 man)            | 100  |
| Tent (personal)         | 50   |
| Twine (300 ft)          | 5    |
| Waterskin               | 5    |
| Whistle                 | 5    |
